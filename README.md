# Latin Sentence Generator

## Project Goals

### Primary Goal

We want to create a program with which a simple, gramatically correct latin sentence can be generated on the fly from the ruleset of the latin language.

### Secondary Goal

To create the foundations of a more complex piece of software which could be used to audit the grammar of latin language.

## Sections

### Deployment

Contained herein are the (BASH) scripts which set up the testing container and test the code.


### Docs

This is the folder for all of the documentation and general files which pertain to the project.

### Media

This is basically for me to put pictures in when I run some code or a command and it does something I really really wanted it to do. If a picture appears in here it means i'm happy. - Andrew

### Reference

The code which spawned this project.

### SRC

The general, day to day codebase.

### Verbal Categories

Linguistic information, referential documents and spreadsheets. Lots of spreadsheets.

## Resources

https://api.verbix.com/
this is an API to some functionality which already does a load of stuff.

http://www.perseus.tufts.edu/hopper/morph?l=haereo&la=la
This is an functionality of the perseus hopper word study tool. It does a part of what we're trying to do. it gets attributes of any word it knows about. AMAZING.

http://www.perseus.tufts.edu/hopper/xmlmorph?lang=la&lookup=haereo
same thing but in a neat parsable XML format.


