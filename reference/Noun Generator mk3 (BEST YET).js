#!/usr/bin/js

//Noun Arrays Beginning//

let oneDeclArray = ['puell', 'femin', 'agricol', 'naut'];
let twoDeclMArray = ['domin', 'ann', 'mur', 'agn'];
let twoDeclNArray = ['bell', 'templ', 'supplici', 'auxili', 'praesidi'];
let threeDeclMFArray = ['reg', 'ens', 'milit', 'for', 'hiem', 'civ'];
let threeDeclNArray = ['corpor', 'limin', 'flumin', 'nomin', 'numin'];
let fourDeclMFArray = ['grad', 'man', 'dom', 'curs'];
let fourDeclNArray = ['corn', 'gen', 'pec', 'spec'];
let fiveDeclArray = ['die', 'spe', 're'];

//Noun Arrays Ending//

//Noun Objects Beginning//

let oneDeclObj = {
  nomS: 'a',
  accS: 'am',
  genS: 'ae',
  datS: 'ae',
  ablS: 'a',
  nomP: 'ae',
  accP: 'as',
  genP: 'arum',
  datP: 'is',
  ablP: 'is',  
};

let twoDeclMObj = {
  nomS: 'us',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'i',
  accP: 'os',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};


let twoDeclNObj = {
  nomS: 'um',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'a',
  accP: 'a',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};

let threeDeclMFObj = {
  nomS: 's',
  accS: 'em',
  genS: 'is',
  datS: 'i',
  ablS: 'e',
  nomP: 'es',
  accP: 'es',
  genP: 'um',
  datP: 'ibus',
  ablP: 'ibus'
};

let threeDeclNObj = {
  nomS: '',
  accS: '',
  genS: 'is',
  datS: 'i',
  ablS: 'e',
  nomP: 'a',
  accP: 'a',
  genP: 'um',
  datP: 'ibus',
  ablP: 'ibus'
};

let fourDeclMFObj = {
  nomS: 'us',
  accS: 'um',
  genS: 'us',
  datS: 'ui',
  ablS: 'u',
  nomP: 'us',
  accP: 'us',
  genP: 'uum',
  datP: 'ibus',
  ablP: 'ibus'
};



let fourDeclNObj = {
  nomS: 'u',
  accS: 'u',
  genS: 'us',
  datS: 'ui',
  ablS: 'u',
  nomP: 'ua',
  accP: 'ua',
  genP: 'uum',
  datP: 'ibus',
  ablP: 'ibus'
};

let fiveDeclObj = {
  nomS: 's',
  accS: 'm',
  genS: 'i',
  datS: 'i',
  ablS: '',
  nomP: 's',
  accP: 's',
  genP: 'rum',
  datP: 'bus',
  ablP: 'bus'
}

//Noun Objects Ending//

//Case creation Functions Beginning//

//1st Declension//
let oneDeclNomS = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.nomS}`;
};
let oneDeclAccS = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.accS}`;
};
let oneDeclGenS = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.genS}`;
};
let oneDeclDatS = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.datS}`;
};
let oneDeclAblS = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.ablS}`;
};
let oneDeclNomP = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.nomP}`;
};
let oneDeclAccP = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.accP}`;
};
let oneDeclGenP = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.genP}`;
};
let oneDeclDatP = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.datP}`;
};
let oneDeclAblP = () => {
  return `${oneDeclArray[Math.floor(Math.random()*oneDeclArray.length)]}${oneDeclObj.ablP}`;
};

//2nd Declension Masculine//
let twoDeclMNomS = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.nomS}`;
};
let twoDeclMAccS = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.accS}`;
};
let twoDeclMGenS = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.genS}`;
};
let twoDeclMDatS = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.datS}`;
};
let twoDeclMAblS = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.ablS}`;
};
let twoDeclMNomP = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.nomP}`;
};
let twoDeclMAccP = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.accP}`;
};
let twoDeclMGenP = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.genP}`;
};
let twoDeclMDatP = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.datP}`;
};
let twoDeclMAblP = () => {
  return `${twoDeclMArray[Math.floor(Math.random()*twoDeclMArray.length)]}${twoDeclMObj.ablP}`;
};

//2nd Declension Neuter//
let twoDeclNNomS = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.nomS}`;
};
let twoDeclNAccS = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.accS}`;
};
let twoDeclNGenS = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.genS}`;
};
let twoDeclNDatS = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.datS}`;
};
let twoDeclNAblS = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.ablS}`;
};
let twoDeclNNomP = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.nomP}`;
};
let twoDeclNAccP = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.accP}`;
};
let twoDeclNGenP = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.genP}`;
};
let twoDeclNDatP = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.datP}`;
};
let twoDeclNAblP = () => {
  return `${twoDeclNArray[Math.floor(Math.random()*twoDeclNArray.length)]}${twoDeclNObj.ablP}`;
};


//3rd Declension Masculine/Feminine//

//Nominitive//

//3rd Declension Masculine/Feminine Nominitive Modifier Function//
let threeDeclMFNomSRoot = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.nomS}`;
};

let nomStr = threeDeclMFNomSRoot();

let nomStr3 = nomStr.substr(nomStr.length-2, nomStr.length-1);

let threeDeclMFNomS = () => {
if (nomStr3 ==='gs') {
  nomStr = nomStr.replace('gs', 'x');
  return nomStr;
} else if (nomStr3 ==='ss') {
  nomStr = nomStr.replace('ss', 'sis');
  return nomStr;
} else if (nomStr3 === 'ts') {
  nomStr = nomStr.replace('its', 'es');
  return nomStr;
} else if (nomStr3 === 'vs') {
  nomStr = nomStr.replace('vs', 'vis');
  return nomStr;
}
};

//End of Nominitive modifier//

let threeDeclMFAccS = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.accS}`;
};
let threeDeclMFGenS = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.genS}`;
};
let threeDeclMFDatS = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.datS}`;
};
let threeDeclMFAblS = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.ablS}`;
};
let threeDeclMFNomP = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.nomP}`;
};
let threeDeclMFAccP = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.accP}`;
};
let threeDeclMFGenP = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.genP}`;
};
let threeDeclMFDatP = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.datP}`;
};
let threeDeclMFAblP = () => {
  return `${threeDeclMFArray[Math.floor(Math.random()*threeDeclMFArray.length)]}${threeDeclMFObj.ablP}`;
};


//3rd Declension Neuter ***NEEDS MUCH WORK AND PROBABLY SUBDIVIDING INTO FAMILIES//

//3rd Declension Neuter Nominitive Modifier Function//
let threeDeclNNomSRoot = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.nomS}`;
};

let nomStr = threeDeclNNomSRoot();

let nomStr3 = nomStr.substr(nomStr.length-2, nomStr.length-1);

let threeDeclNNomS = () => {
if (nomStr3 ==='in') {
  nomStr = nomStr.replace('in', 'en');
  return nomStr;
} else if (nomStr3 ==='or') {
  nomStr = `${nomStr.slice(0, nomStr.length-2)}us`
  return nomStr;
} else if (nomStr3 === 'it') {
  nomStr = nomStr.replace('it', 'ut');
  return nomStr;
} else if (nomStr3 === 'ar') {
  nomStr = nomStr.replace('ar', 'are');
  return nomStr;
} else if (nomStr3 ==='or') {
  nomStr = nomStr.replace('or', 'us');
  return nomStr;
} else if (nomStr3==='er'&& nomStr !=='itiner') {
  nomStr = nomStr.replace('er', 'us');
} else if (nomStr === 'itiner')
  nomStr = 'iter';
  return nomStr;
};
//End of nominitive function

let threeDeclNAccS = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.accS}`;
};
let threeDeclNGenS = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.genS}`;
};
let threeDeclNDatS = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.datS}`;
};
let threeDeclNAblS = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.ablS}`;
};
let threeDeclNNomP = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.nomP}`;
};
let threeDeclNAccP = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.accP}`;
};
let threeDeclNGenP = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.genP}`;
};
let threeDeclNDatP = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.datP}`;
};
let threeDeclNAblP = () => {
  return `${threeDeclNArray[Math.floor(Math.random()*threeDeclNArray.length)]}${threeDeclNObj.ablP}`;
};


//4th Declension Masc & Fem://
let fourDeclMFNomS = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.nomS}`;
};
let fourDeclMFAccS = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.accS}`;
};
let fourDeclMFGenS = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.genS}`;
};
let fourDeclMFDatS = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.datS}`;
};
let fourDeclMFAblS = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.ablS}`;
};
let fourDeclMFNomP = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.nomP}`;
};
let fourDeclMFAccP = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.accP}`;
};
let fourDeclMFGenP = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.genP}`;
};
let fourDeclMFDatP = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.datP}`;
};
let fourDeclMFAblP = () => {
  return `${fourDeclMFArray[Math.floor(Math.random()*fourDeclMFArray.length)]}${fourDeclMFObj.ablP}`;
};


//4th Declension Neuter//

let fourDeclNNomS = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.nomS}`;
};
let fourDeclNAccS = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.accS}`;
};
let fourDeclNGenS = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.genS}`;
};
let fourDeclNDatS = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.datS}`;
};
let fourDeclNAblS = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.ablS}`;
};
let fourDeclNNomP = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.nomP}`;
};
let fourDeclNAccP = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.accP}`;
};
let fourDeclNGenP = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.genP}`;
};
let fourDeclNDatP = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.datP}`;
};
let fourDeclNAblP = () => {
  return `${fourDeclNArray[Math.floor(Math.random()*fourDeclNArray.length)]}${fourDeclNObj.ablP}`;
};


//5th Declension//
let fiveDeclNomS = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.nomS}`;
};
let fiveDeclAccS = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.accS}`;
};
let fiveDeclGenS = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.genS}`;
};
let fiveDeclDatS = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.datS}`;
};
let fiveDeclAblS = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.ablS}`;
};
let fiveDeclNomP = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.nomP}`;
};
let fiveDeclAccP = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.accP}`;
};
let fiveDeclGenP = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.genP}`;
};
let fiveDeclDatP = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.datP}`;
};
let fiveDeclAblP = () => {
  return `${fiveDeclArray[Math.floor(Math.random()*fiveDeclArray.length)]}${fiveDeclObj.ablP}`;
};



console.log('1st Declension:');
console.log('');
console.log(oneDeclNomS());
console.log(oneDeclAccS());
console.log(oneDeclGenS());
console.log(oneDeclDatS());
console.log(oneDeclAblS());
console.log('');
console.log(oneDeclNomP());
console.log(oneDeclAccP());
console.log(oneDeclGenP());
console.log(oneDeclDatP());
console.log(oneDeclAblP());
console.log('');
console.log('');
console.log('2nd Declension Masculine:');
console.log('');
console.log(twoDeclMNomS());
console.log(twoDeclMAccS());
console.log(twoDeclMGenS());
console.log(twoDeclMDatS());
console.log(twoDeclMAblS());
console.log('');
console.log(twoDeclMNomP());
console.log(twoDeclMAccP());
console.log(twoDeclMGenP());
console.log(twoDeclMDatP());
console.log(twoDeclMAblP());
console.log('');
console.log('');
console.log('2nd Declension Neuter:');
console.log('');
console.log(twoDeclNNomS());
console.log(twoDeclNAccS());
console.log(twoDeclNGenS());
console.log(twoDeclNDatS());
console.log(twoDeclNAblS());
console.log('');
console.log(twoDeclNNomP());
console.log(twoDeclNAccP());
console.log(twoDeclNGenP());
console.log(twoDeclNDatP());
console.log(twoDeclNAblP());
console.log('');
console.log('');
console.log('3rd Declension Masc & Fem:');
console.log('');
console.log(threeDeclMFNomS());
console.log(threeDeclMFAccS());
console.log(threeDeclMFGenS());
console.log(threeDeclMFDatS());
console.log(threeDeclMFAblS());
console.log('');
console.log(threeDeclMFNomP());
console.log(threeDeclMFAccP());
console.log(threeDeclMFGenP());
console.log(threeDeclMFDatP());
console.log(threeDeclMFAblP());
console.log('');
console.log('');
console.log('3rd Declension Neuter:');
console.log('');
console.log(threeDeclNNomS());
console.log(threeDeclNAccS());
console.log(threeDeclNGenS());
console.log(threeDeclNDatS());
console.log(threeDeclNAblS());
console.log('');
console.log(threeDeclNNomP());
console.log(threeDeclNAccP());
console.log(threeDeclNGenP());
console.log(threeDeclNDatP());
console.log(threeDeclNAblP());
console.log('');
console.log('');
console.log('4th Declension Masc & Fem:');
console.log('');
console.log(fourDeclMFNomS());
console.log(fourDeclMFAccS());
console.log(fourDeclMFGenS());
console.log(fourDeclMFDatS());
console.log(fourDeclMFAblS());
console.log('');
console.log(fourDeclMFNomP());
console.log(fourDeclMFAccP());
console.log(fourDeclMFGenP());
console.log(fourDeclMFDatP());
console.log(fourDeclMFAblP());
console.log('');
console.log('');
console.log('4th Declension Neuter:');
console.log('');
console.log(fourDeclNNomS());
console.log(fourDeclNAccS());
console.log(fourDeclNGenS());
console.log(fourDeclNDatS());
console.log(fourDeclNAblS());
console.log('');
console.log(fourDeclNNomP());
console.log(fourDeclNAccP());
console.log(fourDeclNGenP());
console.log(fourDeclNDatP());
console.log(fourDeclNAblP());
console.log('');
console.log('');
console.log('5th Declension:');
console.log('');
console.log(fiveDeclNomS());
console.log(fiveDeclAccS());
console.log(fiveDeclGenS());
console.log(fiveDeclDatS());
console.log(fiveDeclAblS());
console.log('');
console.log(fiveDeclNomP());
console.log(fiveDeclAccP());
console.log(fiveDeclGenP());
console.log(fiveDeclDatP());
console.log(fiveDeclAblP());

