#!/usr/bin/js

console.log("Code starting");
/*CODE FOR GENERATING LATIN CLAUSES TAKE 1 */


/* Array of latin nouns in the Nominative case */
const ex1Subj = ['dea', 'femina', 'puella', 'puer', 'vir', 'deus'];

/* Array of latin nouns in the Accusative case, singluar (um/am) and plural (os/as) */
const ex1Obj = ['deam', 'deas', 'feminam', 'feminas', 'puellam', 'puellas', 'puerum', 'puerum', 'virum', 'viros', 'deum', 'deos'];
/* Array of Latin nouns in the present indicative active tense */
const ex1Verb = ['ducit', 'mittit', 'necat', 'oppugnat', 'capit', 'portat']

/* choose a subj at random */
const listEx1Subj=ex1Subj[Math.floor(Math.random()*6)];
/* choose an obj at random */
const listEx1Obj=ex1Obj[Math.floor(Math.random()*12)];
/* choose a verb at random */
const listEx1Verb=ex1Verb[Math.floor(Math.random()*6)];

/* Stick 'em together */
console.log(`${listEx1Subj} ${listEx1Obj} ${listEx1Verb}`);
