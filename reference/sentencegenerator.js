#!/usr/bin/js

/* CODE FOR GENERATING LATIN CLAUSES TAKE 1 */

/* Arrays of pre-conjugated nominative third person singular 'subject' nouns, accusative third person singular and plural 'object' nouns and singular third person active voice verbs.  */
const ex1Subj = ['dea', 'femina', 'puella', 'puer', 'vir', 'deus'];
const ex1Obj = ['deam', 'deas', 'feminam', 'feminas', 'puellam', 'puellas', 'puerum', 'puerum', 'virum', 'viros', 'deum', 'deos'];
const ex1Verb = ['ducit', 'mittit', 'necat', 'oppugnat', 'capit', 'portat']

/* choose a random one of each */
const listEx1Subj=ex1Subj[Math.floor(Math.random()*6)];
const listEx1Obj=ex1Obj[Math.floor(Math.random()*12)];
const listEx1Verb=ex1Verb[Math.floor(Math.random()*6)];


/*  Stick then together */
console.log(`${listEx1Subj} ${listEx1Obj} ${listEx1Verb}`);
