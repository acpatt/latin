#!/usr/bin/js


const verbGenerator = () => {
/* verb root of 'to take */
let verbRoot = 'capi';
/* choose a random ending of the verb from a list of possible endings */
let randN=Math.floor(Math.random()*6);
const listVerbEnding = ['o', 's', 't', 'mus', 'tis', 'nt'];
let verbEnding=listVerbEnding[randN];
/*return the root followed by an ending*/
if (randN === 5) {
  return `${verbRoot}u${verbEnding}`;
} else {
  return `${verbRoot}${verbEnding}`;
};
};

console.log(verbGenerator());
