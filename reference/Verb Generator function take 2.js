#!/usr/bin/js

const conj1 = ['ama', 'para', 'neca'];
const conj2 = ['mone', 'habe', 'gaude'];
const conj4 = ['audi', 'dormi', 'cupi'];

const verbRootArrayGen = () => {
let randN = Math.floor(Math.random()*3);
const verbConjArray = [conj1, conj2, conj4];
  let verbRootArray = verbConjArray[randN];
  return verbRootArray;
  console.log (verbRootArray);
};


const verbRootArrayGlob=verbRootArrayGen();

const verbRootGen = () => {
  let randN = Math.floor(Math.random()*3);
  let randN2 = Math.floor(Math.random()*3);    
if (randN===0) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
} else if (randN===1) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
} else if (randN===2) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
}
};


const verbRootGlob=verbRootGen();


const verbGenerator = () => {
    /* pick a verb root */
let verbRoot = verbRootGlob;
    /* Sometimes we want to chop the last letter off (ama -> am -> amo)*/
let verbRootLess = verbRootGlob.slice(0, -1); //This is for 1st pers sing 1st Conj
  
let randN=Math.floor(Math.random()*6);
const listVerbEnding = ['o', 's', 't', 'mus', 'tis', 'nt'];
    /* choose a random verb ending */
let verbEnding=listVerbEnding[randN];
  //Need to add condition to *below* to require also verbRootArray===conj4
    /* for fourth conjugation present indicative third person plural, it's 'audiunt', rather than 'audint' */
if (verbRootArrayGlob===conj4 && randN===5) {
  return `${verbRoot}u${verbEnding}`;
    /* for first conjugation present indicative first person singular, it's 'amo' rather than 'amao' */
} else if (verbRootArrayGlob===conj1 && randN===0) {
  return `${verbRootLess}${verbEnding}`;
} else {
  return `${verbRoot}${verbEnding}`;
};  
  //Need to add further conditions for: 1) 1st Conj 1st pers sing and 2) 3rd Conj
};

const verb=verbGenerator();

console.log(verb);


