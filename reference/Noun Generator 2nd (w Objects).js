#!/usr/bin/js


//Noun Arrays Beginning//

let oneDeclArray = ['puell', 'femin', 'agricol', 'naut'];
let twoDeclMArray = ['domin'];
let twoDeclNArray = ['bell'];
let threeDeclMFArray = ['reg'];
let threeDeclNArray = ['corpor'];
let fourDeclMFArray = ['grad'];
let fourDeclNArray = ['corn'];
let fiveDeclArray = ['die'];

//Noun Arrays Ending//

//Noun Objects Beginning//

let oneDeclObj = {
  nomS: function makeNomS () {
    return `${oneDeclArray[0]}a`
  },
  accS: function makeAccS () {
    return `${oneDeclArray[0]}am`
  },
  genS: function makeGenS () {
    return `${oneDeclArray[0]}ae`
  },
  datS: function makeDatS () {
    return `${oneDeclArray[0]}ae`
  },
  ablS: function makeAblS () {
    return `${oneDeclArray[0]}a`
  },
  nomP: function makeNomP () {
    return `${oneDeclArray[0]}ae`
  },
  accP: function makeAccP () {
    return `${oneDeclArray[0]}as`
  },
  genP: function makeGenP () {
    return `${oneDeclArray[0]}arum`
  },
  datP: function makeDatP () {
    return `${oneDeclArray[0]}is`
  },
  ablP: function makeAblP () {
    return `${oneDeclArray[0]}is`
  },  
}


let twoDeclMObj = {
  nomS: function makeNomS () {
    return `${twoDeclMArray[0]}us`
  },
  accS: function makeAccS () {
    return `${twoDeclMArray[0]}um`
  },
  genS: function makeGenS () {
    return `${twoDeclMArray[0]}i`
  },
  datS: function makeDatS () {
    return `${twoDeclMArray[0]}o`
  },
  ablS: function makeAblS () {
    return `${twoDeclMArray[0]}o`
  },
  nomP: function makeNomP () {
    return `${twoDeclMArray[0]}i`
  },
  accP: function makeAccP () {
    return `${twoDeclMArray[0]}os`
  },
  genP: function makeGenP () {
    return `${twoDeclMArray[0]}orum`
  },
  datP: function makeDatP () {
    return `${twoDeclMArray[0]}is`
  },
  ablP: function makeAblP () {
    return `${twoDeclMArray[0]}is`
  },  
}


let twoDeclNObj = {
  nomS: function makeNomS () {
    return `${twoDeclNArray[0]}um`
  },
  accS: function makeAccS () {
    return `${twoDeclNArray[0]}um`
  },
  genS: function makeGenS () {
    return `${twoDeclNArray[0]}i`
  },
  datS: function makeDatS () {
    return `${twoDeclNArray[0]}o`
  },
  ablS: function makeAblS () {
    return `${twoDeclNArray[0]}o`
  },
  nomP: function makeNomP () {
    return `${twoDeclNArray[0]}a`
  },
  accP: function makeAccP () {
    return `${twoDeclNArray[0]}a`
  },
  genP: function makeGenP () {
    return `${twoDeclNArray[0]}orum`
  },
  datP: function makeDatP () {
    return `${twoDeclNArray[0]}is`
  },
  ablP: function makeAblP () {
    return `${twoDeclNArray[0]}is`
  },  
}

let threeDeclMFObj = {
  nomS: function makeNomS () {
    return `${threeDeclMFArray[0]}s`    
  },
  accS: function makeAccS () {
    return `${threeDeclMFArray[0]}em`
  },
  genS: function makeGenS () {
    return `${threeDeclMFArray[0]}is`
  },
  datS: function makeDatS () {
    return `${threeDeclMFArray[0]}i`
  },
  ablS: function makeAblS () {
    return `${threeDeclMFArray[0]}e`
  },
  nomP: function makeNomP () {
    return `${threeDeclMFArray[0]}es`
  },
  accP: function makeAccP () {
    return `${threeDeclMFArray[0]}es`
  },
  genP: function makeGenP () {
    return `${threeDeclMFArray[0]}um`
  },
  datP: function makeDatP () {
    return `${threeDeclMFArray[0]}ibus`
  },
  ablP: function makeAblP () {
    return `${threeDeclMFArray[0]}ibus`
  },  
}


let threeDeclNObj = {
  nomS: function makeNomS () {
    return `${threeDeclNArray[0]}`
  },
  accS: function makeAccS () {
    return `${threeDeclNArray[0]}`
  },
  genS: function makeGenS () {
    return `${threeDeclNArray[0]}is`
  },
  datS: function makeDatS () {
    return `${threeDeclNArray[0]}i`
  },
  ablS: function makeAblS () {
    return `${threeDeclNArray[0]}e`
  },
  nomP: function makeNomP () {
    return `${threeDeclNArray[0]}a`
  },
  accP: function makeAccP () {
    return `${threeDeclNArray[0]}a`
  },
  genP: function makeGenP () {
    return `${threeDeclNArray[0]}um`
  },
  datP: function makeDatP () {
    return `${threeDeclNArray[0]}ibus`
  },
  ablP: function makeAblP () {
    return `${threeDeclNArray[0]}ibus`
  },  
}

let fourDeclMFObj = {
  nomS: function makeNomS () {
    return `${fourDeclMFArray[0]}us`
  },
  accS: function makeAccS () {
    return `${fourDeclMFArray[0]}um`
  },
  genS: function makeGenS () {
    return `${fourDeclMFArray[0]}us`
  },
  datS: function makeDatS () {
    return `${fourDeclMFArray[0]}ui`
  },
  ablS: function makeAblS () {
    return `${fourDeclMFArray[0]}u`
  },
  nomP: function makeNomP () {
    return `${fourDeclMFArray[0]}us`
  },
  accP: function makeAccP () {
    return `${fourDeclMFArray[0]}us`
  },
  genP: function makeGenP () {
    return `${fourDeclMFArray[0]}uum`
  },
  datP: function makeDatP () {
    return `${fourDeclMFArray[0]}ibus`
  },
  ablP: function makeAblP () {
    return `${fourDeclMFArray[0]}ibus`
  },  
}



let fourDeclNObj = {
  nomS: function makeNomS () {
    return `${fourDeclNArray[0]}u`
  },
  accS: function makeAccS () {
    return `${fourDeclNArray[0]}u`
  },
  genS: function makeGenS () {
    return `${fourDeclNArray[0]}us`
  },
  datS: function makeDatS () {
    return `${fourDeclNArray[0]}ui`
  },
  ablS: function makeAblS () {
    return `${fourDeclNArray[0]}u`
  },
  nomP: function makeNomP () {
    return `${fourDeclNArray[0]}ua`
  },
  accP: function makeAccP () {
    return `${fourDeclNArray[0]}ua`
  },
  genP: function makeGenP () {
    return `${fourDeclNArray[0]}uum`
  },
  datP: function makeDatP () {
    return `${fourDeclNArray[0]}ibus`
  },
  ablP: function makeAblP () {
    return `${fourDeclNArray[0]}ibus`
  },  
}

let fiveDeclObj = {
  nomS: function makeNomS () {
    return `${fiveDeclArray[0]}s`
  },
  accS: function makeAccS () {
    return `${fiveDeclArray[0]}m`
  },
  genS: function makeGenS () {
    return `${fiveDeclArray[0]}i`
  },
  datS: function makeDatS () {
    return `${fiveDeclArray[0]}i`
  },
  ablS: function makeAblS () {
    return `${fiveDeclArray[0]}`
  },
  nomP: function makeNomP () {
    return `${fiveDeclArray[0]}s`
  },
  accP: function makeAccP () {
    return `${fiveDeclArray[0]}s`
  },
  genP: function makeGenP () {
    return `${fiveDeclArray[0]}rum`
  },
  datP: function makeDatP () {
    return `${fiveDeclArray[0]}bus`
  },
  ablP: function makeAblP () {
    return `${fiveDeclArray[0]}bus`
  },  
}

//Noun Objects Ending//







console.log(oneDeclObj.nomS());
console.log(oneDeclObj.accS());
console.log(oneDeclObj.genS());
console.log(oneDeclObj.datS());
console.log(oneDeclObj.ablS());
console.log('');
console.log(oneDeclObj.nomP());
console.log(oneDeclObj.accP());
console.log(oneDeclObj.genP());
console.log(oneDeclObj.datP());
console.log(oneDeclObj.ablP());
console.log('');
console.log('');
console.log(twoDeclMObj.nomS());
console.log(twoDeclMObj.accS());
console.log(twoDeclMObj.genS());
console.log(twoDeclMObj.datS());
console.log(twoDeclMObj.ablS());
console.log('');
console.log(twoDeclMObj.nomP());
console.log(twoDeclMObj.accP());
console.log(twoDeclMObj.genP());
console.log(twoDeclMObj.datP());
console.log(twoDeclMObj.ablP());
console.log('');
console.log('');
console.log(twoDeclNObj.nomS());
console.log(twoDeclNObj.accS());
console.log(twoDeclNObj.genS());
console.log(twoDeclNObj.datS());
console.log(twoDeclNObj.ablS());
console.log('');
console.log(twoDeclNObj.nomP());
console.log(twoDeclNObj.accP());
console.log(twoDeclNObj.genP());
console.log(twoDeclNObj.datP());
console.log(twoDeclNObj.ablP());
console.log('');
console.log('');
console.log(threeDeclMFObj.nomS());
console.log(threeDeclMFObj.accS());
console.log(threeDeclMFObj.genS());
console.log(threeDeclMFObj.datS());
console.log(threeDeclMFObj.ablS());
console.log('');
console.log(threeDeclMFObj.nomP());
console.log(threeDeclMFObj.accP());
console.log(threeDeclMFObj.genP());
console.log(threeDeclMFObj.datP());
console.log(threeDeclMFObj.ablP());
console.log('');
console.log('');
console.log(threeDeclNObj.nomS());
console.log(threeDeclNObj.accS());
console.log(threeDeclNObj.genS());
console.log(threeDeclNObj.datS());
console.log(threeDeclNObj.ablS());
console.log('');
console.log(threeDeclNObj.nomP());
console.log(threeDeclNObj.accP());
console.log(threeDeclNObj.genP());
console.log(threeDeclNObj.datP());
console.log(threeDeclNObj.ablP());
console.log('');
console.log('');
console.log(fourDeclMFObj.nomS());
console.log(fourDeclMFObj.accS());
console.log(fourDeclMFObj.genS());
console.log(fourDeclMFObj.datS());
console.log(fourDeclMFObj.ablS());
console.log('');
console.log(fourDeclMFObj.nomP());
console.log(fourDeclMFObj.accP());
console.log(fourDeclMFObj.genP());
console.log(fourDeclMFObj.datP());
console.log(fourDeclMFObj.ablP());
console.log('');
console.log('');
console.log(fourDeclNObj.nomS());
console.log(fourDeclNObj.accS());
console.log(fourDeclNObj.genS());
console.log(fourDeclNObj.datS());
console.log(fourDeclNObj.ablS());
console.log('');
console.log(fourDeclNObj.nomP());
console.log(fourDeclNObj.accP());
console.log(fourDeclNObj.genP());
console.log(fourDeclNObj.datP());
console.log(fourDeclNObj.ablP());
console.log('');
console.log('');
console.log(fiveDeclObj.nomS());
console.log(fiveDeclObj.accS());
console.log(fiveDeclObj.genS());
console.log(fiveDeclObj.datS());
console.log(fiveDeclObj.ablS());
console.log('');
console.log(fiveDeclObj.nomP());
console.log(fiveDeclObj.accP());
console.log(fiveDeclObj.genP());
console.log(fiveDeclObj.datP());
console.log(fiveDeclObj.ablP());
