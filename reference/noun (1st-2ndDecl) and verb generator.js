#!/usr/bin/js

//Global Arrays Here//

const decl1Array = ['de', 'femin', 'puell', 'agricol'];
const decl2Array = ['puer', 'vir', 'de', 'domin'];
const decl2NArray = ['bell', 'templ', 'auxili', 'pericul'];
const declensions = [decl1Array, decl2Array, decl2NArray];
const decl1EndSing = ['a', 'a', 'am', 'ae', 'ae', 'a'];
const decl1EndPl = ['ae', 'ae', 'as', 'arum', 'is', 'is'];
const decl2EndSing = ['us', 'e', 'um', 'i', 'o', 'o'];
const decl2EndPl = ['i', 'i', 'os', 'orum','is', 'is'];
const decl2NEndSing = ['um', 'um', 'um', 'i', 'o', 'o'];
const decl2NEndPl = ['a', 'a', 'a', 'orum', 'is', 'is'];




//Noun Case Generators hinc//

const nomSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
} 
};

const nounRootGlob=nounRootGen();
  
  if (nounRootArrayGlob===decl1Array) {
    let nomS=`${nounRootGlob}${decl1EndSing[0]}`;
    return nomS;
} else if (nounRootArrayGlob===decl2Array) {
    let nomS=`${nounRootGlob}${decl2EndSing[0]}`;
    return nomS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let nomS=`${nounRootGlob}${decl2NEndSing[0]}`;
  	return nomS;
}
};

const penNomS=nomSGen();


const penNomSMod=()=>{
  
if (penNomS==='puerus') {
  return 'puer';
} else if (penNomS==='virus') {
  return 'vir';
} else{
  return penNomS
}
};

//End of Nominative Singular Generator//



const vocSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
} 
};

const nounRootGlob=nounRootGen();
  
  if (nounRootArrayGlob===decl1Array) {
    let vocS=`${nounRootGlob}${decl1EndSing[1]}`;
    return vocS;
} else if (nounRootArrayGlob===decl2Array) {
    let vocS=`${nounRootGlob}${decl2EndSing[1]}`;
    return vocS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let vocS=`${nounRootGlob}${decl2NEndSing[0]}`;
  	return vocS;
}
};

const penVocS=vocSGen();


const penVocSMod=()=>{ 
if (penVocS==='puere') {
  return 'puer';
} else if (penVocS==='vire') {
  return 'vir';
} else if (penVocS==='filie') {
  return 'filii';
}else{
  return penVocS
}
};

//End of Vocative Singular Generator//



const accSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
} 
};

const nounRootGlob=nounRootGen();
  
  
  if (nounRootArrayGlob===decl1Array) {
    let accS=`${nounRootGlob}${decl1EndSing[2]}`;
    return accS;
} else if (nounRootArrayGlob===decl2Array) {
    let accS=`${nounRootGlob}${decl2EndSing[2]}`;
    return accS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let accS=`${nounRootGlob}${decl2NEndSing[2]}`;
  	return accS;
}
};

//End of Accusative Singular Generator//



const genSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
} 
};

const nounRootGlob=nounRootGen();
  
  
  if (nounRootArrayGlob===decl1Array) {
    let genS=`${nounRootGlob}${decl1EndSing[3]}`;
    return genS;
} else if (nounRootArrayGlob===decl2Array) {
    let genS=`${nounRootGlob}${decl2EndSing[3]}`;
    return genS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let genS=`${nounRootGlob}${decl2NEndSing[3]}`;
  	return genS;
}
};

//End of Genetive Generator//



const datSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  

  if (nounRootArrayGlob===decl1Array) {
    let datS=`${nounRootGlob}${decl1EndSing[4]}`;
    return datS;
} else if (nounRootArrayGlob===decl2Array) {
    let datS=`${nounRootGlob}${decl2EndSing[4]}`;
    return datS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let datS=`${nounRootGlob}${decl2NEndSing[4]}`;
  	return datS;
}
};


//End of Dative Singular Generator//



const ablSGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  
  
  if (nounRootArrayGlob===decl1Array) {
    let ablS=`${nounRootGlob}${decl1EndSing[5]}`;
    return ablS;
} else if (nounRootArrayGlob===decl2Array) {
    let ablS=`${nounRootGlob}${decl2EndSing[5]}`;
    return ablS;
} else if (nounRootArrayGlob===decl2NArray) {
  	let ablS=`${nounRootGlob}${decl2NEndSing[5]}`;
  	return ablS;
}
};

//End of Ablative Singular Generator//



const nomPGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  
  
  if (nounRootArrayGlob===decl1Array) {
    let nomP=`${nounRootGlob}${decl1EndPl[0]}`;
    return nomP;
} else if (nounRootArrayGlob===decl2Array) {
    let nomP=`${nounRootGlob}${decl2EndPl[0]}`;
    return nomP;
} else if (nounRootArrayGlob===decl2NArray) {
    let nomP=`${nounRootGlob}${decl2NEndPl[0]}`;
    return nomP;
} 
};

//End of Nominative Plural Generator//



const vocPGen = () => {

const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  

  if (nounRootArrayGlob===decl1Array) {
    let vocP=`${nounRootGlob}${decl1EndPl[1]}`;
    return vocP;
} else if (nounRootArrayGlob===decl2Array) {
    let vocP=`${nounRootGlob}${decl2EndPl[1]}`;
    return vocP;
} else if (nounRootArrayGlob===decl2NArray) {
    let vocP=`${nounRootGlob}${decl2NEndPl[1]}`;
    return vocP;
}
};

const penVocP=vocPGen();


const penVocPMod=()=>{ 
if (penVocP==='dei') {
  return 'di';
}else{
  return penVocP
}
};

//End of Vocative Plural Generator//



const accPGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  
  
  if (nounRootArrayGlob===decl1Array) {
    let accP=`${nounRootGlob}${decl1EndPl[2]}`;
    return accP;
} else if (nounRootArrayGlob===decl2Array) {
    let accP=`${nounRootGlob}${decl2EndPl[2]}`;
    return accP;
} else if (nounRootArrayGlob===decl2NArray) {
    let accP=`${nounRootGlob}${decl2NEndPl[2]}`;
    return accP;
}  
};

//End of Accusative Plural Generator//



const genPGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  

  if (nounRootArrayGlob===decl1Array) {
    let genP=`${nounRootGlob}${decl1EndPl[3]}`;
    return genP;
} else if (nounRootArrayGlob===decl2Array) {
    let genP=`${nounRootGlob}${decl2EndPl[3]}`;
    return genP;
} else if (nounRootArrayGlob===decl2NArray) {
    let genP=`${nounRootGlob}${decl2NEndPl[3]}`;
    return genP;
}
};

//End of Genetive Plural Generator//



const datPGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  

  
  if (nounRootArrayGlob===decl1Array) {
    let datP=`${nounRootGlob}${decl1EndPl[4]}`;
    return datP;
} else if (nounRootArrayGlob===decl2Array) {
    let datP=`${nounRootGlob}${decl2EndPl[4]}`;
    return datP;
} else if (nounRootArrayGlob===decl2NArray) {
    let datP=`${nounRootGlob}${decl2NEndPl[4]}`;
    return datP;
}
};

//End of Dative Plural Generator//



const ablPGen = () => {
  
const nounRootArrayGen = () => {
const randDeclArray=Math.floor(Math.random()*declensions.length);  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);
const randDecl2N = Math.floor(Math.random()*decl2NArray.length);
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} else if (nounRootArrayGlob===decl2NArray) {
    let NounRoot=nounRootArrayGlob[randDecl2N];
  		return NounRoot;
}
};

const nounRootGlob=nounRootGen();
  
  
  
  if (nounRootArrayGlob===decl1Array) {
    let ablP=`${nounRootGlob}${decl1EndPl[5]}`;
    return datP;
} else if (nounRootArrayGlob===decl2Array) {
    let ablP=`${nounRootGlob}${decl2EndPl[5]}`;
    return ablP;
} else if (nounRootArrayGlob===decl2NArray) {
    let ablP=`${nounRootGlob}${decl2NEndPl[5]}`;
    return ablP;
}
};

//End of Ablative Plural Generator//



//Case Variables//

const nomS=penNomSMod();
const vocS=penVocSMod();
const accS=accSGen();
const genS=genSGen();
const datS=datSGen();
const ablS=ablSGen();

const nomP=nomPGen();
const vocP=penVocPMod();
const accP=accPGen();
const genP=genPGen();
const datP=datPGen();
const ablP=ablPGen();

console.log(nomS);
console.log(vocS);
console.log(accS);
console.log(genS);
console.log(datS);
console.log(ablS);
console.log(nomP);
console.log(vocP);
console.log(accP);
console.log(genP);
console.log(datP);
console.log(ablP);

// End of Nouns//



//Beginning of Verbs//

const conj1 = ['ama', 'para', 'neca'];
const conj2 = ['mone', 'habe', 'gaude'];
const conj4 = ['audi', 'dormi', 'cupi'];

const verbRootArrayGen = () => {
let randN = Math.floor(Math.random()*3);
const verbConjArray = [conj1, conj2, conj4];
  let verbRootArray = verbConjArray[randN];
  return verbRootArray;
  console.log (verbRootArray);
};


const verbRootArrayGlob=verbRootArrayGen();

const verbRootGen = () => {
  let randN = Math.floor(Math.random()*3);
  let randN2 = Math.floor(Math.random()*3);    
if (randN===0) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
} else if (randN===1) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
} else if (randN===2) {
  let verbRoot=verbRootArrayGlob[randN2];
  return verbRoot;
}
};


const verbRootGlob=verbRootGen();


const verbGenerator = () => {
let verbRoot = verbRootGlob;  
let verbRootLess = verbRootGlob.slice(0, -1); //This is for 1st pers sing 1st Conj
let randN=Math.floor(Math.random()*6);
const listVerbEnding = ['o', 's', 't', 'mus', 'tis', 'nt'];
let verbEnding=listVerbEnding[randN];
  //Need to add condition to *below* to require also verbRootArray===conj4
if (verbRootArrayGlob===conj4 && randN===5) {
  return `${verbRoot}u${verbEnding}`;
} else if (verbRootArrayGlob===conj1 && randN===0) {
  return `${verbRootLess}${verbEnding}`;
} else {
  return `${verbRoot}${verbEnding}`;
};  
  //Need to add further conditions for: 1) 1st Conj 1st pers sing and 2) 3rd Conj
};

const verb=verbGenerator();

console.log(verb);


console.log(`${nomS} ${accS} ${verb}`);

