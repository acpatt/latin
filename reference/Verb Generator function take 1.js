#!/usr/bin/js


/* Function to pick a randon verb root */
const verbRootArrayGen = () => {
    /* These are first conjugation verb roots (regular present infinitive active has '-are' ending - 'portare' ) */
const conj1 = ['ama', 'para', 'neca'];
    /* These are second conjugation verb roots (regular present infinitive active has '-eo' ending - 'docere') */
const conj2 = ['mone', 'habe', 'aude'];
    /* These are fourth conjugation verb roots (regular present infinitive active has '-ire' ending - 'munire') */
const conj4 = ['audi', 'dormi', 'cupi'];

let randN = Math.floor(Math.random()*3);
const verbConjArray = [conj1, conj2, conj4];
  let verbRootArray = verbConjArray[randN];
    /* Return one of the root arrays at random. */
  return verbRootArray;
};


const verbRootGen = () => {
  let randN = Math.floor(Math.random()*3);
  let randN2 = Math.floor(Math.random()*3);
  const verbRootArray = verbRootArrayGen ();
    /* Confusing ifelse statement in which every case does the same thing */
if (randN===0) {
  let verbRoot=verbRootArray[randN2];
  return verbRoot;
} else if (randN===1) {
  let verbRoot=verbRootArray[randN2];
  return verbRoot;
} else if (randN===2) {
  let verbRoot=verbRootArray[randN2];
  return verbRoot;
}
};



const verbGenerator = () => {
    /* Get one verb root at random */
let verbRoot = verbRootGen();  
let randN=Math.floor(Math.random()*6);
const listVerbEnding = ['o', 's', 't', 'mus', 'tis', 'nt'];
    /* Pick a verb ending at random fr0m the list. */
let verbEnding=listVerbEnding[randN];
  //Need to add condition to *below* to require also verbRootArray===conj4
    /* return start plus end of verb (not sure about the 'u') */
if (randN===5) {
  return `${verbRoot}u${verbEnding}`;
} else {
  return `${verbRoot}${verbEnding}`;
};  
  //Need to add further conditions for: 1) 1st Conj 1st pers sing and 2) 3rd Conj
};

console.log(verbGenerator());
