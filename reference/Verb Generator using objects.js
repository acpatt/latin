#!/usr/bin/js
/*VERB GENERATOR USING OBJECTS*/


/* array of verb roots of varying declensions */
let verbArray = ['ama', 'mone', 'reg', 'capi', 'audi'];

 
/* object containing verb inflections for present tense, first second and third person  */
let presAI = {
  firstS: 'o',
  secondS: 's',
  thirdS: 't',
  firstP: 'mus',
  secondP: 'tis',
  thirdP: 'nt'
};

let firstS = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.firstS}`;
  if (verb.substr(verb.length-2, verb.length-1)==='ao') {
    verb = `${verb.slice(0, verb.length-2)}o`;
    return verb
  } else {
    return verb;
  };
};

let thirdS = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdS}`;
  if (verb.substr(verb.length-2, verb.length-1) ==='at' || verb.substr(verb.length-2, verb.length-1) ==='et' ||verb.substr(verb.length-2, verb.length-1) ==='it' ) {
    return verb
  } else {
    verb = `${verb.slice(0, verb.length-1)}it`
    return verb
  };
}

let thirdP = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdP}`;
  if (verb.substr(verb.length-3, verb.length-1) ==='ant' || verb.substr(verb.length-3, verb.length-1) ==='ent') {
    return verb
  } else if (verb.substr(verb.length-3, verb.length-1) ==='int') {
    verb = `${verb.slice(0, verb.length-2)}unt`;
    return verb
  } else {
    verb = `${verb.slice(0, verb.length-2)}unt`
    return verb
  };
}

console.log(firstS());
console.log(thirdS());
console.log(thirdP());
