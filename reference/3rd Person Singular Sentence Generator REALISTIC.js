#!/usr/bin/js

let verbArray = ['aedifica', 'capi', 'move', 'neca', 'porta'];

let presAI = {
  firstS:  'o',
  secondS: 's',
  thirdS:  't',
  firstP:  'mus',
  secondP: 'tis',
  thirdP:  'nt'
};

let firstS = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.firstS}`;
  if (verb.substr(verb.length-2, verb.length-1)==='ao') {
    verb = `${verb.slice(0, verb.length-2)}o`;
    return verb
  } else {
    return verb;
  };
};





let thirdP = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdP}`;
  if (verb.substr(verb.length-3, verb.length-1) ==='ant' || verb.substr(verb.length-3, verb.length-1) ==='ent') {
    return verb
  } else if (verb.substr(verb.length-3, verb.length-1) ==='int') {
    verb = `${verb.slice(0, verb.length-2)}unt`;
    return verb
  } else {
    verb = `${verb.slice(0, verb.length-2)}unt`
    return verb
  };
}


let oneDeclArray = ['agricol', 'aqu'];
let twoDeclMArray = ['amic', 'equ'];
let twoDeclNArray = ['oppid', 'templ', 'aur'];

let nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];


//Noun Objects Beginning//

let oneDeclObj = {
  nomS: 'a',
  accS: 'am',
  genS: 'ae',
  datS: 'ae',
  ablS: 'a',
  nomP: 'ae',
  accP: 'as',
  genP: 'arum',
  datP: 'is',
  ablP: 'is',  
};

let twoDeclMObj = {
  nomS: 'us',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'i',
  accP: 'os',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};


let twoDeclNObj = {
  nomS: 'um',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'a',
  accP: 'a',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};

let thirdPersSentGen = () => {
    let nomDecl = nounArray[Math.floor(Math.random()*nounArray.length)]
    let nomNounRoot = nomDecl[Math.floor(Math.random()*nomDecl.length)]
  
  let nomGenerator = () => {

    if (nomDecl === oneDeclArray) {
      return `${nomNounRoot}${oneDeclObj.nomS}`;
    } else if (nomDecl === twoDeclMArray) {
      return `${nomNounRoot}${twoDeclMObj.nomS}`;
    } else if (nomDecl === twoDeclNArray) {
      return `${nomNounRoot}${twoDeclNObj.nomS}`;
    }
  }
  
  let nominative = nomGenerator();
  
  
    if (nominative==='agricola') {
      verbArray = ['aedifica', 'move', 'porta'];
    } else if (nominative==='aqua') {
      verbArray = ['move'];
    } else if (nominative==='amicus') {
      verbArray = ['aedifica', 'capi', 'move', 'neca', 'porta'];
    } else if (nominative==='equus') {
      verbArray = ['neca', 'porta'];
    } else if (nominative==='oppidum') {
      verbArray = ['move'];
    } else if (nominative==='templum') {
      verbArray = ['move'];
    } else if (nominative==='aurum') {
      verbArray = ['move'];
    };

  let thirdS = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdS}`;
  if (verb.substr(verb.length-2, verb.length-1) ==='at' || verb.substr(verb.length-2, verb.length-1) ==='et' ||verb.substr(verb.length-2, verb.length-1) ==='it' ) {
    return verb
  } else {
    verb = `${verb.slice(0, verb.length-1)}it`
    return verb
  };
}
  
  let verb = thirdS();
  
  // THIS SECTION CAUSES NOM AND ACC RETURN AS 'UNDEFINED' // 
  
  if (nominative==='agricola' && verb === 'aedificat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    twoDeclNArray = ['oppid', 'templ'];
    oneDeclArray = ['MISSING WORD'];
    twoDeclMArray = ['MISSING WORD'];
  } else if (nominative==='agricola' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['aqu'];
    twoDeclMArray = ['amic', 'equ'];
    twoDeclNArray = ['aur'];
  } else if (nominative==='agricola' && verb === 'portat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['aqu'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['aur'];
  } else if (nominative==='aqua' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    twoDeclNArray = ['oppid', 'templ'];
    oneDeclArray = ['MISSING WORD'];
    twoDeclMArray = ['MISSING WORD'];
  } else if (nominative==='amicus' && verb === 'aedificat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    twoDeclNArray = ['oppid', 'templ'];
    oneDeclArray = ['MISSING WORD'];
    twoDeclMArray = ['MISSING WORD'];
  } else if (nominative==='amicus' && verb === 'capit') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['equ'];
    twoDelNArray = ['oppid'];
  } else if (nominative==='amicus' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['aqu'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['MISSING WORD'];
  } else if (nominative==='amicus' && verb === 'necat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic', 'equ'];
  } else if (nominative==='amicus' && verb === 'portat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['aqu'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['aur'];
  } else if (nominative==='equus' && verb === 'necat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic', 'equ'];
    twoDeclNArray = ['MISSING WORD'];
  } else if (nominative==='equus' && verb === 'portat') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['MISSING WORD'];
  } else if (nominative==='aurum' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['MISSING WORD'];
  } else if (nominative==='oppidum' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['MISSING WORD'];
  } else if (nominative==='templum' && verb === 'movet') {
    nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray];
    oneDeclArray = ['agricol'];
    twoDeclMArray = ['amic'];
    twoDeclNArray = ['MISSING WORD'];
  }
  
  let accGenerator = () => {
    let accDecl = nounArray[Math.floor(Math.random()*nounArray.length)]
    let accNounRoot = accDecl[Math.floor(Math.random()*accDecl.length)]
    if (accDecl === oneDeclArray) {
      return `${accNounRoot}${oneDeclObj.accS}`;
    } else if (accDecl === twoDeclMArray) {
      return `${accNounRoot}${twoDeclMObj.accS}`;
    } else if (accDecl === twoDeclNArray) {
      return `${accNounRoot}${twoDeclNObj.accS}`;
    }
  }
  
  let accusative = accGenerator();
  
  

  return `${nominative} ${accusative} ${verb}`;
  
    }

console.log(thirdPersSentGen());










