#!/usr/bin/js

/* Arrays of nouns with associated endings */ 
const decl1Array = ['de', 'femin', 'puell', 'agricol'];
const decl2Array = ['puer', 'vir', 'de', 'domin'];
const declensions = [decl1Array, decl2Array];
const decl1EndSing = ['a', 'a', 'am', 'ae', 'ae', 'a'];
const decl1EndPl = ['ae', 'ae', 'as', 'arum', 'is', 'is']
const decl2EndSing = ['us', 'e', 'um', 'i', 'o', 'o'];
const decl2EndPl = ['i', 'i', 'os', 'orum','is', 'is'];

const randDeclArray=Math.floor(Math.random()*declensions.length);
const randDecl1 = Math.floor(Math.random()*decl1Array.length);
const randDecl2 = Math.floor(Math.random()*decl2Array.length);

const nounRootArrayGen = () => {
  
  let nounRootArray = declensions[randDeclArray];
  return nounRootArray
};

const nounRootArrayGlob=nounRootArrayGen();



const nounRootGen = () => {
if (nounRootArrayGlob===decl1Array) {
    let NounRoot= nounRootArrayGlob[randDecl1];
      return NounRoot;
} else if (nounRootArrayGlob===decl2Array) {
    let NounRoot= nounRootArrayGlob[randDecl2];
      return NounRoot;
} 
};

const nounRootGlob=nounRootGen();


const nomSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let nomS=`${nounRootGlob}${decl1EndSing[0]}`;
    return nomS;
} else if (nounRootArrayGlob===decl2Array) {
    let nomS=`${nounRootGlob}${decl2EndSing[0]}`;
    return nomS;
  }
};

const penNomS=nomSGen();


const penNomSMod=()=>{
  
if (penNomS==='puerus') {
  return 'puer';
} else if (penNomS==='virus') {
  return 'vir';
} else{
  return penNomS
}
};

const nomS=penNomSMod();
console.log(nomS);



//End of Nominative Singular Generator//



const vocSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let vocS=`${nounRootGlob}${decl1EndSing[1]}`;
    return vocS;
} else if (nounRootArrayGlob===decl2Array) {
    let vocS=`${nounRootGlob}${decl2EndSing[1]}`;
    return vocS;
  }
};

const penVocS=vocSGen();


const penVocSMod=()=>{ 
if (penVocS==='puere') {
  return 'puer';
} else if (penVocS==='vire') {
  return 'vir';
} else if (penVocS==='filie') {
  return 'filii';
}else{
  return penVocS
}
};

const vocS=penVocSMod();
console.log(vocS);



//End of Vocative Singular Generator//



const accSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let accS=`${nounRootGlob}${decl1EndSing[2]}`;
    return accS;
} else if (nounRootArrayGlob===decl2Array) {
    let accS=`${nounRootGlob}${decl2EndSing[2]}`;
    return accS;
  }
};

const accS=accSGen();
console.log(accS);



//End of Accusative Singular Generator//



const genSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let genS=`${nounRootGlob}${decl1EndSing[3]}`;
    return genS;
} else if (nounRootArrayGlob===decl2Array) {
    let genS=`${nounRootGlob}${decl2EndSing[3]}`;
    return genS;
  }
};

const genS=genSGen();
console.log(genS);


//End of Genetive Generator//


const datSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let datS=`${nounRootGlob}${decl1EndSing[4]}`;
    return datS;
} else if (nounRootArrayGlob===decl2Array) {
    let datS=`${nounRootGlob}${decl2EndSing[4]}`;
    return datS;
  }
};

const datS=datSGen();
console.log(datS);


//End of Dative Singular Generator//


const ablSGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let ablS=`${nounRootGlob}${decl1EndSing[5]}`;
    return ablS;
} else if (nounRootArrayGlob===decl2Array) {
    let ablS=`${nounRootGlob}${decl2EndSing[5]}`;
    return ablS;
  }
};

const ablS=ablSGen();
console.log(ablS);


//End of Ablative Singular Generator//


const nomPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let nomP=`${nounRootGlob}${decl1EndPl[0]}`;
    return nomP;
} else if (nounRootArrayGlob===decl2Array) {
    let nomP=`${nounRootGlob}${decl2EndPl[0]}`;
    return nomP;
  }
};

const nomP=nomPGen();
console.log(nomP);


//End of Nominative Plural Generator//


const vocPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let vocP=`${nounRootGlob}${decl1EndPl[1]}`;
    return vocP;
} else if (nounRootArrayGlob===decl2Array) {
    let vocP=`${nounRootGlob}${decl2EndPl[1]}`;
    return vocP;
  }
};

const penVocP=vocPGen();


const penVocPMod=()=>{ 
if (penVocP==='dei') {
  return 'di';
}else{
  return penVocP
}
};

const vocP=penVocPMod();
console.log(vocP);


//End of Vocative Plural Generator//


const accPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let accP=`${nounRootGlob}${decl1EndPl[2]}`;
    return accP;
} else if (nounRootArrayGlob===decl2Array) {
    let accP=`${nounRootGlob}${decl2EndPl[2]}`;
    return accP;
  }
};

const accP=accPGen();
console.log(accP);


//End of Accusative Plural Generator//


const genPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let genP=`${nounRootGlob}${decl1EndPl[3]}`;
    return genP;
} else if (nounRootArrayGlob===decl2Array) {
    let genP=`${nounRootGlob}${decl2EndPl[3]}`;
    return genP;
  }
};

const genP=genPGen();
console.log(genP);


//End of Genetive Plural Generator//


const datPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let datP=`${nounRootGlob}${decl1EndPl[4]}`;
    return datP;
} else if (nounRootArrayGlob===decl2Array) {
    let datP=`${nounRootGlob}${decl2EndPl[4]}`;
    return datP;
  }
};

const datP=datPGen();
console.log(datP);


//End of Dative Plural Generator//


const ablPGen = () => {
  if (nounRootArrayGlob===decl1Array) {
    let ablP=`${nounRootGlob}${decl1EndPl[5]}`;
    return datP;
} else if (nounRootArrayGlob===decl2Array) {
    let ablP=`${nounRootGlob}${decl2EndPl[5]}`;
    return ablP;
  }
};

const ablP=ablPGen();
console.log(ablP);


//End of Ablative Plural Generator//





const subj=penSubjMod();
console.log(subj);
