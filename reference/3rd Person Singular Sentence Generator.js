#!/usr/bin/js
//THERE IS IN THIS VERSION NO RANDOMISATION BETWEEN SINGULAR AND PLURAL, AND NO MODIFICATION OF ARRAYS TO CREATE REALISTIC SENTENCES.

let verbArray = ['aedifica' ,'ambula', 'ama', 'audi', 'bib', 'canta', 'capi', 'clama', 'cupi', 'constitu', 'consum', 'curr', 'da', 'dele', 'dic', 'disced', 'dormi', 'duc', 'faci', 'festina', 'habe', 'habita', 'iaci', 'intra', 'iube', 'labora', 'lauda', 'leg', 'lud', 'mane', 'mitt', 'mone', 'move', 'naviga', 'neca', 'oppugna', 'ostend', 'para', 'pon', 'porta', 'pugna', 'responde', 'reg', 'ride', 'roga', 'scrib', 'specta', 'supera', 'tene', 'terre', 'time', 'veni', 'vide', 'voca'];

let presAI = {
  firstS:  'o',
  secondS: 's',
  thirdS:  't',
  firstP:  'mus',
  secondP: 'tis',
  thirdP:  'nt'
};






let thirdP = () => {
  let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdP}`;
  if (verb.substr(verb.length-3, verb.length-1) ==='ant' || verb.substr(verb.length-3, verb.length-1) ==='ent') {
    return verb
  } else if (verb.substr(verb.length-3, verb.length-1) ==='int') {
    verb = `${verb.slice(0, verb.length-2)}unt`;
    return verb
  } else {
    verb = `${verb.slice(0, verb.length-2)}unt`
    return verb
  };
}


let oneDeclArray = ['agricol', 'ancill', 'aqu', 'de', 'femin', 'fili', 'hast', 'incol', 'insul', 'ir', 'naut', 'patri', 'pecuni', 'poet', 'puell', 'regin', 'sagitt', 'terr', 'turb', 'und', 'vi'];
let twoDeclMArray = ['amic', 'cib', 'de', 'equ', 'fili', 'gladi', 'loc', 'mur', 'nunti', 'Roman', 'serv', 'soci', 'vent'];
let twoDeclNArray = ['aur', 'auxili', 'bell', 'cael', 'oppid', 'pericul', 'proeli', 'scut', 'templ', 'verb', 'vin'];

let nounArray = [oneDeclArray, twoDeclMArray, twoDeclNArray]


//Noun Objects Beginning//

let oneDeclObj = {
  nomS: 'a',
  accS: 'am',
  genS: 'ae',
  datS: 'ae',
  ablS: 'a',
  nomP: 'ae',
  accP: 'as',
  genP: 'arum',
  datP: 'is',
  ablP: 'is',  
};

let twoDeclMObj = {
  nomS: 'us',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'i',
  accP: 'os',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};


let twoDeclNObj = {
  nomS: 'um',
  accS: 'um',
  genS: 'i',
  datS: 'o',
  ablS: 'o',
  nomP: 'a',
  accP: 'a',
  genP: 'orum',
  datP: 'is',
  ablP: 'is',
};

let thirdPersSentGen = () => {
  let nomGenerator = () => {
    let nomDecl = nounArray[Math.floor(Math.random()*nounArray.length)]
    let nomNounRoot = nomDecl[Math.floor(Math.random()*nomDecl.length)]
    if (nomDecl === oneDeclArray) {
      return `${nomNounRoot}${oneDeclObj.nomS}`;
    } else if (nomDecl === twoDeclMArray) {
      return `${nomNounRoot}${twoDeclMObj.nomS}`;
    } else if (nomDecl === twoDeclNArray) {
      return `${nomNounRoot}${twoDeclNObj.nomS}`;
    }
  }
  let thirdS = () => {
    let verb = `${verbArray[Math.floor(Math.random()*verbArray.length)]}${presAI.thirdS}`;
    if (verb.substr(verb.length-2, verb.length-1) ==='at' || verb.substr(verb.length-2, verb.length-1) ==='et' ||verb.substr(verb.length-2, verb.length-1) ==='it' ) {
      return verb
    } else {
      verb = `${verb.slice(0, verb.length-1)}it`
      return verb
    };
}

  let accGenerator = () => {
    let accDecl = nounArray[Math.floor(Math.random()*nounArray.length)]
    let accNounRoot = accDecl[Math.floor(Math.random()*accDecl.length)]
    if (accDecl === oneDeclArray) {
      return `${accNounRoot}${oneDeclObj.accS}`;
    } else if (accDecl === twoDeclMArray) {
      return `${accNounRoot}${twoDeclMObj.accS}`;
    } else if (accDecl === twoDeclNArray) {
      return `${accNounRoot}${twoDeclNObj.accS}`;
    }
  }
  
  return `${nomGenerator()} ${accGenerator()} ${thirdS()}`;
  
    }

console.log(thirdPersSentGen());
