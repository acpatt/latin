# try again

Clause:
    id 80000
    type 'simple with actor'
    structure [Word,Word,Word]

    generate()
    getStructure()
    populateRoots()
    declinjugate()
    toString() -> "WordString WordString WordString"


Word:
    id 80000
    value 'virum'
    role 'clauseSubject'

    generate()
    getRoot()
    toString() -> "WordString"

InflectedWord:
    id 80786
    value 'virum'
    role 'clauseSubject'
    root Root
    inflection Inflection

    generate()
    getRoot()
    toString() -> "WordString"

Noun:
    id 80786
    value 'virum'
    role 'clauseSubject'
    root NounRoot
    inflection NounInflection

    generate()
    getRoot()
    toString() -> "WordString"

Verb:
    id 80456
    value 'amabant'
    role 'clauseMainVerb'
    root VerbRoot
    inflection VerbInflection

    generate()
    getRoot()
    toString() -> "WordString"


Root:
    id 28765
    root 'root'
    meaning 'the bit under a tree'


NounRoot:
    id 28986
    root 'root'
    meaning 'the bit under a tree'
    declension 1
    paradigm_noun 'paradigm'
    gender 'gender'

VerbRoot:
    id 28765
    root 'root'
    meaning 'the bit under a tree' 
    conjugation '0'
    paradigm 'sum'
    infinitive 'esse'
    perfect_root 'fu'
    perfect_passive_participle 'act'
    regularity 'regular'

Inflection: 
    id 20786
    inflection 'o'
    plurality 'plural'
    paradigm 'amo'

NounInflection:
    id 20786
    inflection 'o'
    plurality 'plural'
    paradigm 'puella'
    declension 1
    gender 'mf'
    case 'nominative'

VerbInflection:
    id 20738
    inflection 'o'
    plurality 'plural'
    paradigm 'amo'
    conjugation 1
    tense 'present'
    voice 'active'
    mood 'indicative'
    person 1

Role: 
    id 21932
    role 'numinative subject'
    wordType 'Noun'

Structure: 
    id 21343
    type 'Simple Transitive Active + Direct Object'
    roleArray ['nominative subject' => Role,'accusative object' => Role,'transitive verb' => Role]
    


