# Datastructures

## Introduction

This is a document detailing the various datastructures we will need to think of in the process of generating latin. A datastructure can be loosely defined here as a set of rules which govern the relationships between sets of data and elements within a set. For instance, take a calendar of holidays. The calendar has all of the days of the year - [0,1,2,..,363,364], as well as the months - [Jan,Feb,...,Nov,Dec] and then a list of holidays, along with which day they fall on - [(New Years Day,0),(Easter,94),...,(Boxing Day,360),(New Year's Eve,364)]. 

The 'Calendar' datastructure is the mapping of the Holidays to days, and days to months:

|Holiday|Day|Month|
|---|---|---|
|New Years Day|0|Jan|
|   |1|Jan|
|   |2|Jan|
|...|...|...|
|   |93|Mar|
|Easter|94|Mar|
|   |95|Mar|
|...|...|...|
|...|...|...|


In this example the datastructure is simply a 2 dimensional table, but it will get more interesting.

## Structures

### Pronoun Roots

|ID|PronounRoot|Person|
|--|---|---|

This will need to be filled.

### Noun Roots

A useful table of noun roots will have a noun root, along with its declension, gender, Paradigm noun and meaning. These are the values which are not dependent on any context and can be - in principle - input directly with the root. A convenient table arranged with such columns is available.

|ID|Root|Declension|Gender|Paradigm Noun|Meaning|
|---|---|---|---|---|---|
|0|agricol|first|masculine|puella|farmer|
|1|amic|first|feminine|puella|"girl-friend, female friend"|
|2|ancill|first|feminine|puella|"slave-girl, slave-woman"|
|3|reg|third|masculine|rex|king|

A Paradigm noun is a noun which defines which class of regularity a noun root falls into. The logic is that for all noun roots, the algorithm for going from the noun root to the conjugated noun in the nominative singular case is identical to that of its associated paradigm noun. 

### Noun Root Tags

We want to narrow down which subjects can do which verbs to what objects. In order to start this, we first need a table of noun tags. In order to make the process as granular as possible, we will define subtags under tags to further separate possible verbs. An extremely basic example set of tags is defined here.


|ID|NounRootTag|Parent ID|
|---|---|---|
|0|living_things|NULL|
|1|places|NULL|
|2|objects|NULL|
|3|animals|0|
|4|people|3|
|5|plants|0|
|6|royalty|4|
|7|professionals|4|
|8|places_on_land|1|
|9|military_members|7|
|...|...|...|

This table would look as follows if presented visually:

![nounclasses](./files/documentation_media/nounclasses.png)

### The Link Table - Noun Roots to Noun Root Tags

In order to associate noun roots with noun root tags, we define a new 'NounRoots_NounRootTags' table, which simple holds an entry for every tag associated with every root. 
(For illustrative purposes I display here the names, but in actuality the database will hold a mapping of root IDs to tag IDs.)

|Noun Root ID|Noun Root Tag ID|
|---|---|
|agricol|professionals|
|amic|people|
|reg|military_members|
|reg|royalty|

See that a king has two tags and hence two rows in the link table. This is one way in which a database stores a relationship between two tables.

Identical logic can be applied to verbs.

### Intransitive Verbs Tagging.

This is the simplest starting case. First I define a simple example table of verbroots, verb tags and a link table:


|ID|Verb Root|transitivity|
|---|---|---|
|0|dic|intransitive|
|1|clam|intransitive|
|2|susurr|intransitive|
|3|mor|intransitive|
|4|relinq|intransitive|
|5|regna|intransitive|
|6|canta|intransitive|
|7|cadi|intransitive|

speak, cry (like cry out, loud), whisper, die, leave, reign, sing. 

and we define a table of all verb tags (obviously back-of-an-envelope selection here)

|ID|Verb Root Tag|Parent ID|
|---|---|---|
|0|action|NULL|
|1|imposition|NULL|
|2|expression|0|
|3|speech|1|
|4|movement|0|

(that's all I can think of based on the verb table)

then we define a link table, as with noun roots.

|Verb Root|Verb Root Tag|
|---|---|
|dic|speech|
|clam|speech|
|susurr|speech|
|mor|imposition|
|relinq|action|
|regna|action|
|canta|expression|
|cadi|movement|

I've used IDs for ease. 

### Noun Tag to Verb Tag Mapping

Now I can define a noun tag to verb tag mapping based on which classes of nouns can do which classes of verbs. It is here that the complexity occurs. the issue here is that the heirarchy works backwards. 
Speech is a subset of expression, 
    which is a subset of action.
A professional is a subset of people, 
    which is a subset of living things. 

If a living thing can die, that means that a professional can die. In other words, Professionals 'inherit' the abilities of their parent tags (inheriting speech from their parent - people, and inheriting death from their grandparent - living things)

However, looking at verb tags, this works the other way around. If a class of nouns can do 'speech' actions, then they can surely do 'expression' actions and indeed 'action' actions. However, the reverse is not true. 

As such, we have to be careful about our approach to mapping noun tags to verb tags, making sure we always start with the most _general_ noun tag and try to map it to the most _specific_ verb tag, then move to less specific verb tags until there are no more verb tags to assign to that noun tag, then move onto the next noun tag.

There is a way for me to make this process easy for you to do, if you think it's worth the time it would take me. If my thought process is correct then the way to speed the tagging process up is to basically write the plausibility selection code and then run it based on which tag you select, in order to eliminate the redundant tags.

(I NEED TO FINISH THIS BIT)


|Noun Tag|Verb Tag|
|living_things|
|places|
|objects|
|animals|
|people|
|plants|
|royalty|
|professionals|
|places_on_land|
|military_members|



