# Latin Documentation

## Purpose

To contain various pieces of project documentation

## Method

### First Step

the intention is to press a button and receive a sentence in latin.

The ambiguity is of the complexity of that sentence. I'm aware that If we start off with the intention for that sentence to be a three word (maybe two word?) clause then we are limiting ourselves, but I think that this at least will provide an achievable first step to creating something greater.


