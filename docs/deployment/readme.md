# Deployment


## 0.0 What are the steps to having full working CI/CD?

### 1. Running Up a testing docker service

#### 1.1. having database server functionality and node runtime and updated packages.

1.1.1. have a container which is pre-updated and ready to take the 'initialisedatabase' script.

    * learn how to create custom docker image - ref: http://crosbymichael.com/dockerfile-best-practices-take-2.html
DONE
    * learn how to store images in the hub
DONE
    * update gitlab-ci to incorporate this
DONE
    * automatically update this image every ~ week?
PENDING

1.1.2. have a script which checks that everyhing is installed
DONE

    * check that mysql is installed. 
    * check that node is installed.
    * check that everything is mostly up to date

#### 1.3. movement of the code to all the relevant places.

1.3.1. decide upon the folder layout of the deployed code.
DONE
    * CSVs have to be in /var/lib/mysql-files
    * Source code goes in /var/node/latin/[function] - /var/node/latin/deployment, database code in /var/node/latin/database 
    * Deployment and test code in /home/latin/initialise and /home/latin/testng
    * Script to move code to relevant places


1.3.2. script to identify the necessary permissions and set them all on the code, as well as on the database files.
DONE

#### 1.4. movement from a container to a service of multiple containers.
PENDING

### 2. Testing the code

#### 2.1. Implement Gitlab's 'code coverage' statistics

2.1.1. Investigate Gitlab's 'code coverage' functionality.

#### 2.2. Decide upon units of behaviour within the 'simple clause generation' paradigm. 

### 3. Pushing code to dev server

#### 3.1. Schematic for user, folder, database, access.

### 4. Implementing gitlab tags to push code to Production

## 0.1 How will we tolerate the fluid structure of the database?

### 0.1.1. The database will be reinitialised upon every build.

Investigate the prior export of runnig context information (generated clauses, statistics)

### Delayed Tasks

#### 1.2. having a repository from which to retrieve containers.
1.2.1. investigate the repository functionality of gitlab.
