Class Structure questions:


    1. If a simple clause does not have an object, is the verb necessarily intransitive?
Not necessarily. Only certain verbs can be intransitive. For example, if you took a verb such as ‘hit’; if you didn’t have an object for it then it simply wouldn’t make sense. When we come to classify verbs full as we did with nouns this will be one of the attributes we will denote for each verb.

    2. Can a simple clause’s object be a pronoun.
Yes, a pronoun functions exactly the same as a noun and can take any role in a sentence that a noun would take. There are a number of pronouns in Latin which we will go through but I wanted to get nouns done first.

    3. Can I assume there is a one-to-one mapping between singular and plural pronouns.
Yes, there are, at a basic level, 3 types of personal pronoun: 1st person, 2nd person and 3rd person. Within each person there is plurality, so 1st person singular is “I”, while 1st person plural is “We”, 2nd person sing is “you”, 2nd person plural is “you” (English doesn’t differentiate between the two in 2nd person. 3rd person singular is “he/she/it”, 3rd person plural is “they”.

    4. Will there be predefined mapping of possible object nouns to subject nouns and possible verbs to subject/object pairs?
Yes, to a certain degree. I am still not quite sure how to tackle this matter of creating realistic subject/object/verb combos. Since we have a limited vocabulary list (A level), it would be technically possible. However, I did it with a much smaller basic list and it soon became pretty endless individually defining all the possible pairs of nouns and verbs etc. It also means that if we wanted to expand the vocabulary of the sentence generator we would have to do the process all over again to sort out the predefined mappings. My current thinking is that we could separate the nouns into broad categories. 

E.g. Human, king, sailor, farmer, queen etc could be categorised as ‘people’; temple, walls, building, street, inn could be categorised as ‘buildings’; make, destroy, build, construct, pile up could be categorised as ‘fabrication’ and so on. All the nouns within the category of ‘people’ can all broadly do the same things to buildings, so there wouldn’t be a need to manually predefine every single combo.
Inevitably this will throw up some weird combinations, but is there a way where we can get the program to ‘learn’ which ones work and which don’t?
