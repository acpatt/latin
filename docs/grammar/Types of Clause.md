Clause Classes

Intransitive Clauses
    • Simple
        ◦ The man stands
        ◦ vir stat

    • Simple Motion/Location with Preposition
        ◦ The man walks to the forum
        ◦ vir ad forum ambulat

    • Simple with Dative
        ◦ The man believes in god
        ◦ vir deo credit

    • Simple with Adjective
        ◦ The mountain is steep
        ◦ mons arduus est

    • Simple with Comparative Adjective + quam clause
        ◦ Hercules is braver than a lion
        ◦ hercules fortior est quam leo

    • Simple with Comparative Adjective + ablative noun
        ◦ Hercules is braver than a lion
        ◦ hercules fortior leone est

    • Simple with adjective + indirect object
        ◦ The men must destroy the city (literally “the city is, for the men, to be destroyed”)
        ◦ urbs viris delenda est
        ◦ poems are pleasing to mankind
        ◦ carmina hominibus grata sunt


Transitive Clauses
    • Simple
        ◦ The slave kills the lord
        ◦ servus dominum necat

    • Simple with Indirect Object
        ◦ The lord gives freedom to the slave (or “the lord gives the slave his freedom)
        ◦ dominus servo libertatem dat
        ◦ N.B. The lord takes freedom away from the slave (Dative of disadvantage)
        ◦ dominus servo liberatem aufert

    • Simple with Preposition
        ◦ The king leads the citizens towards the forum
        ◦ rex cives ad forum ducit

Passive Transitive Clauses
    • Simple
        ◦ I am killed
        ◦ necar

    • Simple with instrument
        ◦ I am killed by a spear
        ◦ necar hasta

    • Simple with agent
        ◦ I am killed by a barbarian
        ◦ necar ab barbaro

    • Simple with preposition (+ instrument/agent)
        ◦ The citizens are led to the forum by the king
        ◦ cives ad forum ab rege ducuntur
        ◦ The ship is driven to the land by the wind
        ◦ navis vento ad terram agitur


Free Radical elements
These elements can be added to the clause to add layers of complexity (e.g. denoting time, possession, cause, result etc) to the basic meaning. Some elements are completely radical (can appear anywhere), others are more situational.
    • Genitive
        ◦ Noun in genitive case can be added to any noun to denote possession (usually before the noun it possesses).
            ▪ I see the king’s soldiers
            ▪ video regis milites
            ▪ I walk to the temple of the gods
            ▪ ad deorum templum ambulo

    • Adjective
        ◦ Can be added to any noun to describe it (usually after noun)
            ▪ The brave man kills the big lion
            ▪ vir fortis leonem magnum necat

    • Adverb
        ◦ Can be added to any verb to describe it (usually at beginning of clause)
            ▪ The woman runs quickly
            ▪ celeriter femina currit

    • ‘Cum’ prepositional phrase
        ◦ Can be added to any subject or object to denote doing an action, or receiving an action, together with something.
            ▪ The king goes to the forum with the citizens
            ▪ rex cum civibus ad forum it
            ▪ The Romans kill the women along with the children
            ▪ Romani feminas cum liberis necant

    • Ablative/’ab’ prepositional phrase
        ◦ Can be added to any adjective (when adding an agent (with ‘ab’ preposition), or when adding an instrument (with ablative noun) i.e. when an adjective is behaving like a passive participle).
            ▪ Cicero was famous for his speeches (lit. “he was famous by his speeches)
            ▪ Cicero orationibus notus erat
            ▪ The Romans were astonished by the Britons
            ▪ Romani ab Bretonnibus attoniti erant

    • Subordinate Clauses (See Separate Document: “Subordinate Clause Classes)
        ◦ Location of subordinate Clauses is dependent upon which type of subordinate clause is being deployed and thus, I suspect, requires its own ruleset.
