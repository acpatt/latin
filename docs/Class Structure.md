# Structure of the classes

I want to lay out the structure of the program, as per 'Basic Clause Construction'. I will be deliberately vague about the database functionality as the definition thereof is not within the scope of this document.

## Outstanding Questions

* root tag classes to be considered.

* how does the ClauseSubject know it's the subject? 'enum role'?

## class SimpleClause

This will be a class which deals with the generation of clauses. It will have a 'generateClause' method. 

### method generateClause
parameters: enum type, enum Syllabus

connect to the database.

if type is simple, then call generateSimpleClause

### method generateSimpleClause
Parameters: enum Syllabus

Variables:
    enum Explicitness,
    enum Transitivity.
    enum Subject Plurality

* this method returns a string containing a random simple clause e.g. "canes hominem amabant" using only the vocab defined in the Syllabus


call generateSimpleClauseRoots with the explicitness and transitivity, Subject Plurality and Syllabus, which returns an array of word objects.

call DeclinjugateClauseRoots with the array of word objects. 


### method generateSimpleClauseRoots
Parameters: 
    enum Explicitness,
    enum Transitivity.
    enum Subject Plurality
    enum Syllabus

Return:
    array ClauseRoots.

* this method returns an array of wordroot objects containing a random simple clause's word roots. The possibly return values are: [ClauseSubject,ClauseObject,ClauseMainVerb]
[ClauseSubject,ClauseMainVerb]
[ClauseObject,ClauseMainVerb]
[ClauseMainVerb]

this method chooses at random an explicitness (implicit, explicit), transitivity (intransitive, transitive) and calls getSimpleClauseStructure with these values, which returns an array of word ENUMs: One of the following

[verb] 
[object,verb]
[subject,verb]
[subject,object,verb]
  
# The Logic Hereafter within generateSimpleClauseRoots will be dependent on the class implementation, and can be later discussed. For now? It's coding time.


now we have either a subject root and an intransitive verb, or a subject root, object root and a transitive verb.

return an array with the ClauseSubject, ClauseObject and ClauseMainVerb. 

### method getSimpleClauseStructure
Parameters: 
    enum Explicitness, 
    enum Transitivity.


CLAUSE STRUCTURES:

|Explicitness|Transitivity|Clause Structure|
|---|---|---|
|Implicit|Intransitive|[verb]|
|Implicit|Transitive|[object,verb]|
|Explicit|Intransitive|[subject,verb]|
|Explicit|Transitive|[subject,object,verb]|

Return:
    array ClauseStructure.

* this method takes the parameters and returns an array of word objects which governs the structure of the clause e.g. [subject, object, verb]

the method will retrieve from the database an array of word ENUMs (e.g. [subject, object, verb]). It will then return this array. 

### method getClauseSubject
Parameters:
    enum Explicitness.
    enum Syllabus
        
Return:
    (nounroot or pronounroot) ClauseSubject.
    
* This method returns a nounroot or pronounroot based on the explicitness.

If the explicitness is implicit then choose a pronoun from the database at random which is in the syllabus. return it.

Else, choose a noun from the database which is in the Syllabus with the given plurality. Return it.

### getClauseObject
Parameters:
    (nounroot or pronounroot) ClauseSubject,
    enum Syllabus.

Return:
    (nounroot or pronounroot) ClauseObject.
    
    
* This method takes ClauseSubject and returns ClauseObject.

select from teh database a random nounroot (or pronounroot) ClauseObject which is in the Syllabus

###  getClauseMainVerb 
Parameters:
    enum Transitivity,
    (nounroot or pronounroot) ClauseSubject,
    (nounroot or pronounroot) ClauseObject.
    enum Syllabus
    
Return: 
    verbroot MainVerb.
    
* this method generates a verb when given a value of Transitivity, Syllabus, a subject noun and possibly an object noun. 

if the transitivity is intransitive, then select from the database a random intransitive verbroot MainVerbRoot in the Syllabus. Return MainVerbRoot

if the transitivity is transitive, then select from the database a random transitive verb MainVerbRoot in the Syllabus. Return MainVerb.

### declinjugateClauseRoots
Parameters: array [subject, (maybe object?), verb]

Variables:    
    * `SubjectExplicitness`
    * `VerbTransitivity`
    * `SubjectNumber`
    * `ObjectNumber`
    * array `DeclinjugatedSimpleClause` (containing anything from just a verb, to two nouns or pronouns and a verb)
* This method takes an array of words and returns a fully declinjugated clause in the form of an array of noun and or pronoun and or verb objects.

Generate a random `SubjectNumber`.
Check if the subject is implicit (with `isSubjectImplicit(ClauseSubject)`). 
Check if the Verb is Transitive (with `isVerbTransitive(ClauseMainVerb`)).

Randomise the subject number.
If the Subject is Explicit:
        call `ClauseSubject.decline` with the `SubjectNumber` Parameter. This returns a correctly declined `ClauseSubject` noun object. Append this to `DeclinjugatedSimpleClause`. 
If the Verb is Transitive:
    Call `ClauseObject.decline` (if there is one) with the `ObjectNumber` parameter. This returns a correctly declined `ClauseObject`. Append this to `DeclinjugatedSimpleClause`.     
call `ClauseMainVerb.conjugate` with the `SubjectNumber` and `ClauseSubject.Person` parameters.This returns a correctly conjugated `ClauseMainVerb` verb object. Append this to `DeclinjugatedSimpleClause`.
return `DeclinjugatedSimpleClause`.
    

    





