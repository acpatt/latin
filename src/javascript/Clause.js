class Clause {
    constructor() {
        this._clauseID = createClauseID();
        this._clauseStructure = [];
        this._clauseType = "";
        this._clauseBody = [];
    }

    get clauseID() {
        return this._clauseID;
    }
    get clauseStructure() {
        return this._clauseStructure;
    }
    get clauseType() {
        return this._clauseType;
    }
    get clauseBody() {
        return this._clausBody;
    }
    set clauseType(type) {
        if (this._clauseType === "") {
            if (checkClauseType(type)) {
                this._clausetype = type;
            }
            else {
                throw(new error("Invalid Clause Type Passed"));
            }
        }
        else {
            throw(new error("Clause Type is already set!"));
        }

    }
    set clauseStructure(structure) {
        if (this._clauseStructure === []) {
            if (Array.isArray(structure)){
                this._clauseStructure = structure;
            }
            else {
                throw(new error("Structure passed isn't an array!"));
            }
        }
        else {
            throw(new error("Clause Structure is already set!"));
        }
    }

    createClauseID() {
        function getRandomInt(min, max) {
            return min + Math.floor(Math.random() * (max - min + 1));
        }
        var timestamp = 1000*Date.now() + getRandomInt(0,999);
        var sql = `INSERT INTO Clauses (Timestamp) VALUES ('${timestamp}')`;
        LatinDB.pool.query(sql, function (err, result)) {
            if (err) throw err;
            return result.insertID;
        }
    }


}
