# Database Schema:

There is a Database 'Latin' containing tables:
 * 'Declensions' - Two columns: 'Id' and 'Declension'
    - Five rows:
    - 'first'
    - 'second'
    - 'third'
    - 'fourth'
    - 'fifth'
 * 'Genders' - Two columns: 'Id' and 'Gender'
    - Four rows:
    - 'masculine'
    - 'feminine'
    - 'neuter'
    - 'queer'
 * 'Regularities' - Two columns: 'Id' and 'Regularity'
    - _currently_ Three rows:
    - 'regular'
    - 'firstirregular'
    - 'secondirregular'
 * 'NounRoots' - Four columns: 'NounRoot', 'Declension', 'Regularity', 'Gender' and 'Meaning'
    - As many rows as there are roots.
    
 * 'NounCases' - Two columns: 'Id' and 'NounCase'
    - Seven Rows:
    - 'nominative'
    - 'genitive'
    - 'dative'
    - 'accusative' 
    - 'ablative' 
    - 'locative'
    - 'vocative'
 
 * 'Pluralities' - Two columns: 'Id' and 'Plurality'
    - Two Rows:
    - 'singular'
    - 'plural'

 * 'Conjugations' - Two columns: 'Id' and 'Conjugation'
    - Four rows:
    - 'first'
    - 'second'
    - 'third'
    - 'fourth'
    
 