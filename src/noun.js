
class noun {
    constructor(root, clauseRole) {
        this._root = root;
        this._declension = getNounDeclension(root);
        this._regularity = getNounRegularity(root);
        this._gender = getNounGender(root);
        this._clauseRole = clauseRole;
        this._person = 3;
        this._meaning = getNounMeaning(root);
    }
    get root() {
        return this._root;
    }
    get declension() {
        return this._declension;
    }
    get regularity() {
        return this._regularity;
    }
    get gender() {
        return this._gender;
    }

    get clauseRole() {
        return this._clauseRole;
    }
    get person() {
        return this._person;
    }
    get meaning() {
        return this._meaning()
    }

    getNounDeclension(root) {

    }
    decline(nounCase,plurality) {
        if (this._regularity == regularity.REGULAR) {
            return `${this._root}${getInflection(this._declension,this._regularity,this._gender,nounCase,plurality,)}`
        }
    }

}
