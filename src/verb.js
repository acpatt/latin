class verb {
    constructor(root) {
        this._root = root;
        this._infinitive = getInfinitive(root);
        this._perfectRoot = getPerfectRoot(root);
        this._conjugation = getConjugation(root);
    }
    get root() {
        return this._root;
    }

    get infinitive() {
        return this._infinitive;
    }

   get PerfectRoot() {
        return this._perfectRoot;
    }

    get conjugation() {
        return this._conjugation;
    }
    conjugate(root, tense) {
        if (this._tense == tense.perfect || tense.futurePerfect || tense.pluperfect) {
            return `${this._perfectRoot}${getInflection(this._conjugation,this._tense,this._voice,this._mood,this._person)}`
        } else {
            return `${this._Root}${getInflection(this._conjugation,this._tense,this._voice,this._mood,this._person)}`
        }
    }

}
