const gender {
    MASCULINE: 'masculine',
    FEMININE: 'feminine',
    NEUTER: 'neuter',
    QUEER: 'queer'
}

const nounCase {
    NOMINATIVE: 'nominative',
    GENITIVE: 'genitive',
    DATIVE: 'dative',
    ACCUSATIVE: 'accusative',
    ABLATIVE: 'ablative',
    LOCATIVE: 'locative',
    VOCATIVE: 'vocative',
}

const regularity {
    REGULAR: 'regular',
    FIRSTIRREGULAR: 'firstirregular',
    SECONDIRREGULAR: 'secondirregular'
    /* more irregularities canbe defined thus */
}

const plurality {
    SINGULAR: 'singular',
    PLURAL: 'plural'
}

const declension {
    FIRST: 'first'
    SECOND: 'second'
    THIRD: 'third'
    FOURTH: 'fourth'
    FIFTH: 'fifth'
}
