# Deployment

This is a directory of scripts which will run on the deployment server and set up the environment for the program to run - 
such as installing mySQL, Nodejs, npm etc. 

## environment

This contains the scripts required to setup an environment with the packages we need to run Latin.

The first script 'install-dependencies.sh' is a 'BASH' script which installs the linux software packages on which the program depends.

install-packages.sh installed the 'node' packages which give us extra javascript code to work with.
 
## database/

this contains a script to fill an empty database with the initial information it needs. This includes tables and their contents. 

