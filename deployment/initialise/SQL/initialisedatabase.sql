CREATE DATABASE Latin;

USE Latin;
/* Create a table for the possible declensions of a noun */
CREATE TABLE Declensions (
    Id  INTEGER PRIMARY KEY NOT NULL,
    Declension    VARCHAR(6)
);
INSERT INTO Declensions(Id, Declension) VALUES (1,'first');
INSERT INTO Declensions(Id, Declension) VALUES (2,'second');
INSERT INTO Declensions(Id, Declension) VALUES (3,'third');
INSERT INTO Declensions(Id, Declension) VALUES (4,'fourth');
INSERT INTO Declensions(Id, Declension) VALUES (5,'fifth');


/* Create a table for the genders of a noun */
CREATE TABLE Genders (
    Id  INTEGER PRIMARY KEY NOT NULL,
    Gender    VARCHAR(9)
);
INSERT INTO Genders(Id,Gender) VALUES (1,'masculine');
INSERT INTO Genders(Id,Gender) VALUES (2,'feminine');
INSERT INTO Genders(Id,Gender) VALUES (3,'neuter');
INSERT INTO Genders(Id,Gender) VALUES (4,'mascfem');
INSERT INTO Genders(Id,Gender) VALUES (5,'all');


/* Create a table for whether a word is regular, or falls into another of a toBeDefined set of sets of common conjugation named
firstirregular, secondirregular, etc */
CREATE TABLE Regularities (
    Id INTEGER PRIMARY KEY NOT NULL,
    Regularity    VARCHAR(20)
);
INSERT INTO Regularities(Id,Regularity) Values (1,'regular');
INSERT INTO Regularities(Id,Regularity) Values (2,'firstirregular');
INSERT INTO Regularities(Id,Regularity) Values (3,'secondirregular');



/* Create a table for the roots of nouns, the table contains the root, declension, regularity, gender and meaning of the root. */
CREATE TABLE `NounRoots` (
    Id  INTEGER PRIMARY KEY NOT NULL,
    NounRoot    VARCHAR(25),
    Declension  VARCHAR(6)  NOT NULL REFERENCES Declensions(Declension),
    Regularity  VARCHAR(20) NOT NULL REFERENCES Regularities(Regularity),
    Gender  VARCHAR(9) NOT NULL REFERENCES Genders(Gender),
    Meaning VARCHAR(256)
);

/*
NounRoots Table created
*/

/* Create a table for the possible cases of a noun */
CREATE TABLE NounCases (
    Id INTEGER PRIMARY KEY NOT NULL,
    NounCase    VARCHAR(10)
);
INSERT INTO NounCases(Id,NounCase) VALUES (1,'nominative');
INSERT INTO NounCases(Id,NounCase) VALUES (2,'genitive');
INSERT INTO NounCases(Id,NounCase) VALUES (3,'dative');
INSERT INTO NounCases(Id,NounCase) VALUES (4,'accusative');
INSERT INTO NounCases(Id,NounCase) VALUES (5,'ablative');
INSERT INTO NounCases(Id,NounCase) VALUES (6,'locative');
INSERT INTO NounCases(Id,NounCase) VALUES (7,'vocative');

/* Create a table for the possibly pluralities of a noun */
CREATE TABLE Pluralities (
    Id INTEGER PRIMARY KEY NOT NULL,
    Plurality    VARCHAR(10)
);
INSERT INTO Pluralities(Id,Plurality) Values (1,'singular');
INSERT INTO Pluralities(Id,Plurality) Values (2,'plural');


/* Create a table for the possible conjugations of a verb */
CREATE TABLE `Conjugations` (
    Id  INTEGER PRIMARY KEY NOT NULL,
    Conjugation     VARCHAR(7)
);
/* Insert into the table the conjugations */
INSERT INTO Conjugations(Id,Conjugation) VALUES (1,'first');
INSERT INTO Conjugations(Id,Conjugation) VALUES (2,'second');
INSERT INTO Conjugations(Id,Conjugation) VALUES (3,'third');
INSERT INTO Conjugations(Id,Conjugation) VALUES (4,'fourth');


/* Inflections depend on the parameters defined below */
CREATE TABLE `NounInflections` (
    Id  INTEGER PRIMARY KEY NOT NULL,
    Inflection  VARCHAR(6)  NOT NULL,
    Declension  VARCHAR(6)  NOT NULL REFERENCES Declensions(Declension),
    Regularity  VARCHAR(20) NOT NULL REFERENCES Regularities(Regularity),
    Gender  VARCHAR(9) NOT NULL REFERENCES Genders(Gender),
    NounCase    VARCHAR(10) NOT NULL REFERENCES NounCases(NounCase),
    Plurality   VARCHAR(10) NOT NULL REFERENCES Pluralities(Plurality)
);

CREATE TABLE `ParadigmNouns` (
    Id INTEGER PRIMARY KEY NOT NULL,
    Declension VARCHAR(6)  NOT NULL REFERENCES Declensions(Declension),
    ParadigmNoun VARCHAR(10) NOT NULL
);
