#!/bin/bash
export DEBIAN_FRONTEND="noninteractive"
LOGFILE=$LOG/database.log
SPACE="

COMMAND HAS RUN


STARTING NEW COMMAND

"

mysql -e "source $INITIALISE/SQL/initialisedatabase.sql" || exit 1;
echo $SPACE >> $LOGFILE;
echo "Database Initialised";

mysql -e "source $INITIALISE/SQL/csvimport.sql" || exit 1;
echo $SPACE >> $LOGFILE;
echo "CSVs Imported";

