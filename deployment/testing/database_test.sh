#!/bin/bash
export DEBIAN_FRONTEND="noninteractive"
LOGFILE=$LOG/db_test.log
INDB="ohno"
pwd;
TABLES=(Declensions Genders Regularities NounRoots NounCases Pluralities Conjugations NounInflections ParadigmNouns)

echo "Checking Tables in Latin"
for i in "${TABLES[@]}"
do
   echo "Checking Existence of $i table.";
   INDB=$(mysql -e "select table_name from information_schema.tables where table_schema = 'Latin' and table_name = '$i'" | grep "$i");
   echo "$INDB"
   if [ "$INDB" != "$i" ]
   then
      echo "Table: $i does not exist!";
      exit 1;
   fi
   echo "Table: $i created successfully";
   # or do whatever with individual element of the array
done
